# SBB for iDPP@CLEF 2022  #

This repository contains the runs, resources, and code, of the `SBB Team`, University of Padua, Italy, participating in the [iDPP@CLEF 2022](https://brainteaser.health/open-evaluation-challenges/idpp-2022/)
community effort. 

### Organisation of the repository ###

The repository is organised as follows:

* `submission`: this folder contains the runs submitted for the different tasks.
* `score`: this folder contains the performance scores of the submitted runs.

iDPP@CLEF 2022 consists of *two tasks* 

* **Task 1** - Ranking Risk of Impairment
* **Task 2** - Predicting Time of Impairment

Therefore, the `submission` and `score` folders are organized into sub-folders for each task.

### Reference ###

For an explanation of the developed approaches and for citing them, please, refer to:

Trescato, I., Guazzo, A., Longato, E., Hazizaj, E., Roversi, C., Tavazzi, E., Vettoretti, M., and Di Camillo, B. (2022). Baseline Machine Learning Ap- proaches To Predict Amyotrophic Lateral Sclerosis Disease Progression. In Faggioli, G., Ferro, N., Hanbury, A., and Potthast, M., editors, _CLEF 2022 Working Notes_, pages 1277–1293. CEUR Workshop Proceedings (CEUR-WS.org), ISSN 1613-0073. [http://ceur-ws.org/Vol-3180/](http://ceur-ws.org/Vol-3180/).

### License ###

All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). 

![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

